#!/usr/bin/env python3
#! coding:utf-8
import os
import tkinter
import sys
sys.path.append('/opt/rtcds/userapps/release/sys/common/guardian')
import cdslib
import buildlib
import subprocess
from tkinter import messagebox
from epics import caget, caput, cainfo
from datetime import datetime

FEMODELS = [(fe.name, fe.DCUID) for fe in list(cdslib.get_all_models())]
FEMODELS.sort(key=lambda x: x[1])

#models = [(name, dcuid) for (name, dcuid) in FEMODELS if model_in[2:] in name]
hostnames = ['k1lsc0','k1asc0','k1als0','k1ioo','k1ioo1','k1imc0','k1pr2','k1pr0','k1prm','k1mcf0','k1bs','k1sr2','k1sr3','k1srm','k1omc0','k1omc1','k1ix1','k1iy1','k1ex1','k1ey1','k1ex0','k1ey0','k1ipx1','k1test0','k1iy0']
#hostnames = buildlib.getHostnameList()

ifo='k1'
action_list = ['make ','make install-','start']

cblist = [0] * 128
check_value_list = [0] * 128
cbAction = [0] * 3
action_value_list = [0] * 3

directory = os.path.dirname(os.path.abspath(__file__))
logfile = directory+'/autobuild.log'

def write_logfile(comment):
    d = datetime.now()
    with open(logfile,'a') as f:
        f.write(d.strftime('%Y-%m-%d %H:%M:%S ')+os.uname()[1]+', '+comment+'\n')

def btn_click_build():
    global action_value_list, check_value_list

    directory = os.path.dirname(os.path.abspath(__file__))
    #print(directory)
    filepath = directory+'/build_guiout.txt'
    with open(filepath,'w') as output_file:

        for (model, fec) in FEMODELS:
            if check_value_list[fec].get()==True:
                #print(model)
                output_file.write('#'+'\n'+'# '+model+'\n'+'#'+'\n')

                for index, action in enumerate(action_list):
                    if action_value_list[index].get()==True:
                        print(action)
                        output_str = action + ifo + model + '\n'
                    else:
                        output_str = '# ' + action  + ifo + model + '\n'

                    output_file.write(output_str)

                output_file.write('\n')

    view = subprocess.Popen(['emacs',filepath])
    ret = messagebox.askquestion("Start build","Check the build_guiout.txt.\nAre you ready?")
    view.terminate()
    if ret == 'yes':
        write_logfile('start build')
        build = subprocess.Popen(['./autobuild.py',filepath])
        
    else:
        ret = messagebox.showwarning("Build Information","The build was successfully canceled.")

def btn_click_UpdateWebView():
    global check_value_list

    directory = os.path.dirname(os.path.abspath(__file__))
    print(directory)
    filepath = directory+'/update_guiout.sh'
    with open(filepath,'w') as output_file:

        output_file.write('#!/bin/bash\n')
        for (model, fec) in FEMODELS:
            if check_value_list[fec].get()==True:
                output_str = '/opt/rtcds/userapps/release/cds/common/scripts/model_export.sh --execute ' + ifo + model + '\n'
                output_file.write(output_str)

    os.chmod(filepath,0o744)

    view = subprocess.Popen(['emacs',filepath])
    ret = messagebox.askquestion("Update Web Viwer","Check the update_guiout.sh\n\nAre you ready?")
    view.terminate()
    if ret == 'yes':
        write_logfile('Update Web Viewer')
        build = subprocess.Popen(['./update_guiout.sh',filepath])
    else:
        ret = messagebox.showwarning("Update Information","The Update Web Viewer was successfully canceled.")

def btn_click_LoadTable():
    global check_value_list

    ret = messagebox.askquestion("SDF Reload","Are you ready?")
    if ret == 'yes':
        for (model, fec) in FEMODELS:
            if check_value_list[fec].get()==True:
                # K1:FEC-xxx_SDF_DROP_CNT
                channel = ifo.upper() + ':FEC-'+str(fec)+'_SDF_DROP_CNT'
                sdf_drop_cnt = caget(channel)

                if sdf_drop_cnt > 0:
                    # K1:FEC-xxx_SDF_RELOAD
                    channel = ifo.upper() + ':FEC-'+str(fec)+'_SDF_RELOAD'
                    write_logfile('SDF Reload: caput '+channel+' '+str(2))
                    caput(channel,2)   # 2:Release Message
                else:
                    write_logfile('SDF Reload: None: '+channel+',  SDF_DROP_CNT: '+str(sdf_drop_cnt))

    else:
        ret = messagebox.showwarning("SDF Reload","The SDF Reload was successfully canceled.")

def btn_click_DiagReset():
    global check_value_list

    ret = messagebox.askquestion("Diag Reset","Are you ready?")
    if ret == 'yes':
        for (model, fec) in FEMODELS:
            if check_value_list[fec].get()==True:
                # K1:FEC-xxx_DIAG_RESET
                channel = ifo.upper() + ':FEC-'+str(fec)+'_DIAG_RESET'
                write_logfile('Diag Reset: put '+channel+' '+str(1))
                caput(channel,1)    # 1:Release Message
    else:
        ret = messagebox.showwarning("Diag Reset","The Diag Reset was successfully canceled.")


def btn_click_WDDACKillReset():
    global check_value_list
    optic_list = ['mci','mce','mco','immt1','immt2','ommt1','ommt2','ostm','tmsx','tmsy','pr2','pr3','prm','bs','sr2','sr3','srm','itmx','itmy','etmx','etmy']
    optic_tp_list = ['pr2','pr3','prm','bs','sr2','sr3','srm','itmx','itmy','etmx','etmy']
    ret = messagebox.askquestion("[VIS] WD & DK Reset","Are you ready?")
    if ret == 'yes':
        for (model, fec) in FEMODELS:
            if check_value_list[fec].get()==True:
                if 'vis' in model and not 'iop' in model and not 'mon' in model:
                    #print( [x in model for x in optic_list])
                    if True in [x in model for x in optic_list]:
                        wd_dk_reset = False
                        optic = model[3:]
                        #print([x in model for x in optic_tp_list])
                        if True in [x in model for x in optic_tp_list]:
                            optic = optic[:-1]

                            if model[-2:] == 'p':
                                tp = 'PAY'
                            else:
                                tp = 'TWR'

                            # K1:VIS-optic_TWR_MASTERSWITCH
                            # K1:VIS-optic_PAY_MASTERSWITCH
                            channel = ifo.upper() + ':VIS-' + optic.upper() + '_'  + 'TWR_MASTERSWITCH' 
                            master_switch_twr = caget(channel,1)    # 1:Press Message
                            print(channel,master_switch_twr)
                            channel = ifo.upper() + ':VIS-' + optic.upper() + '_'  + 'PAY_MASTERSWITCH' 
                            master_switch_pay = caget(channel,1)    # 1:Press Message
                            print(channel,master_switch_pay)
                            write_logfile('WD and DAC Kill Reset: get '+optic.upper()+', '+str(master_switch_twr)+', '+str(master_switch_pay))

                            if master_switch_twr == 'OFF' and master_switch_pay == 'OFF':
                                wd_dk_reset = True

                        else:
                            tp = 'PAY'
                            channel = ifo.upper() + ':VIS-' + optic.upper() + '_'  + 'PAY_MASTERSWITCH' 
                            master_switch_pay = caget(channel,1)    # 1:Press Message
                            print(channel,master_switch_pay)
                            write_logfile('WD and DAC Kill Reset: get '+optic.upper()+', '+str(master_switch_pay))

                            if  master_switch_pay == 'OFF':
                                wd_dk_reset = True

                        if wd_dk_reset == True:
                            # K1:VIS-ETMX_WD_RESET
                            # K1:VIS-ETMX_TWR_DACKILL_RESET
                            # K1:VIS-ETMX_PAY_DACKILL_RESET
                            write_logfile('WD and DAC Kill Reset: Reset '+optic.upper())

                            channel = ifo.upper() + ':VIS-' + optic.upper() + '_WD_RESET' 
                            print(channel,1)
                            caput(channel,1)    # 1:Press Message
                            if True in [x in model for x in ['tmsx','tmsy']]:
                                channel = ifo.upper() + ':VIS-' + optic.upper() + '_DACKILL_RESET' 
                            else:
                                channel = ifo.upper() + ':VIS-' + optic.upper() + '_'  + tp + '_DACKILL_RESET' 
                            print(channel,1)
                            caput(channel,1)    # 1:Press Message

    else:
        ret = messagebox.showwarning("WD and DAC Kill Reset","The Reset was successfully canceled.")

def main():

    tk = tkinter.Tk()
    tk.geometry('1040x900')
    tk.title('Select Build Target')

    # Initialize
    for fec in range(0,128):
        check_value_list[fec] = tkinter.BooleanVar()

    for index, action in enumerate(action_list):
        action_value_list[index] = tkinter.BooleanVar()

    xpos_start = 10
    xpos = xpos_start
    ypos = 0
    yheight = 18
    width = 200
    ypos_top = yheight

    # TARGET
    lf = tkinter.LabelFrame(tk, width=width*4, height=850, text='Build Target')
    lf.pack(ipadx=10, ipady=10, padx=50, pady=50)
    #lb = tkinter.Label(text='Build Target')
    lf.place(x=xpos,y=ypos)
    ypos=ypos_top
    xpos=xpos+10

    for hostname in hostnames:
        lf = tkinter.LabelFrame(tk, width=width-20, height=2, text=hostname)
        lf.pack(ipadx=10, ipady=10, padx=50, pady=50)

    #    lb = tkinter.Label(text=hostname)
    #    lb.place(x=xpos,y=ypos)
        lf.place(x=xpos,y=ypos)
        ypos=ypos+yheight

        count = 0
        for (model, fec) in FEMODELS:
            build_hostname = buildlib.getmodel2hostname(ifo+model)
            if build_hostname == hostname:
                label = '['+str(fec)+'] '+ifo+model

                cb = tkinter.Checkbutton(tk,text=label,variable=check_value_list[fec])
                cb.place(x=xpos+10, y=ypos)
                #print(cb,fec)
                cblist[fec] = cb
    
                ypos=ypos+yheight
                count+=1

        lf.place(height=yheight*(count+1)+10)

        ypos=ypos+yheight
        if ypos > 700:
            ypos = ypos_top
            xpos = xpos + width

    # BUILD
    xpos = xpos_start * 2 + width * 4
    ypos = 0
    lf2 = tkinter.LabelFrame(tk, width=width, height=yheight*6, text='Build')
    lf2.pack(ipadx=10, ipady=10, padx=50, pady=50)
    #lb2 = tkinter.Label(text='Action')
    lf2.place(x=xpos,y=ypos)
    ypos = ypos_top

    for index, action in enumerate(action_list):
        cb = tkinter.Checkbutton(tk,text=action, variable=action_value_list[index])
        cb.place(x=xpos+10, y=ypos)
        cbAction[index] = cb
        ypos=ypos+yheight

    # EXECUTE
    btn = tkinter.Button(tk, text='Execute', command = btn_click_build)
    btn.place(x=xpos+10,y=ypos)
    ypos=ypos+yheight * 3

    # OTHER
    lf3 = tkinter.LabelFrame(tk, width=width, height=yheight*8, text='Other')
    lf3.pack(ipadx=10, ipady=10, padx=50, pady=50)
    #lb3 = tkinter.Label(text='Other Action')
    lf3.place(x=xpos,y=ypos)
    ypos=ypos+yheight

    btn = tkinter.Button(tk, text='SDF:LOAD TABLE', command = btn_click_LoadTable)
    btn.place(x=xpos+10,y=ypos)
    ypos=ypos+yheight*2

    btn = tkinter.Button(tk, text='[VIS] WD & DAC kill reset', command = btn_click_WDDACKillReset)
    btn.place(x=xpos+10,y=ypos)
    ypos=ypos+yheight*2

    btn = tkinter.Button(tk, text='Update Web Viewer', command = btn_click_UpdateWebView)
    btn.place(x=xpos+10,y=ypos)
    ypos=ypos+yheight*2


    tk.mainloop()

if __name__ == "__main__":
    main()