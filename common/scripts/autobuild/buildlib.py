
import subprocess
from datetime import datetime

import sys
#print(sys.path)
sys.path.append('/opt/rtcds/userapps/release/sys/common/guardian')
import cdslib
import ezca as ezca2

ezca=ezca2.Ezca()
FEMODELS = [(fe.name, fe.DCUID) for fe in list(cdslib.get_all_models())]
ifo='k1'
#hoge='prmt'

filename = '/opt/rtcds/kamioka/k1/target/gds/param/testpoint.par'
systems = dict() 
hostnames = []

# Logfile
LogFileDirectory = '/kagra/Dropbox/Subsystems/VIS/Scripts/AutoBuild/LogFiles/'

def initialize():
    f = open(filename,'r')

    lines = f.readlines()
    newdict = False
    system = ''
    hostname = ''
    for line in lines:
        if line[0] == '[':
            newdict = True

        if newdict == True:
            if line.find('hostname=',0) == 0:
                hostname = line[9:-1]
                if hostname not in hostnames:
                    hostnames.append(hostname)

            if line.find('system=',0) == 0:
                system = line[7:-1]

                # Not including vis
                #if line.find('vis') == -1:
                #    newdict = False
                #    system = ''
                #    hostname = ''
            # `registerto systems
            if hostname != '' and system != '':
                systems[system] = hostname
                newdict = False
                system = ''
                hostname = ''

def getmodel2hostname(modelname):
    if len(systems) == 0:
        initialize()
    return systems[modelname]

def getHostnameList():
    if len(systems) == 0:
        initialize()
    return hostnames

def getmodels(sysname,optic):
  return [(name, dcuid) for (name, dcuid) in FEMODELS if sysname + optic in name]

def isbuildcheck(model_in):
    '''
    String :: model name
    -> Int :: exit code
    '''

    models = [(name, dcuid) for (name, dcuid) in FEMODELS if model_in[2:] in name]

    for (model, fec) in models:
        '''
        Condition:
            Guardian state == SAFE
            --Commish status == OFF--
            Commish message == NULL
            SDF diff == 0
            CFC bit == OFF
        '''
        if model_in[2:5] == 'vis':
            optic_tp = model_in[len('k1vis'):].lower()
            if optic_tp[-1:] == 't' or optic_tp[-1:] == 'p':
                optic = optic_tp[:-1]
            else:
                optic = optic_tp

            flag = ezca['GRD-VIS_' + optic.upper() + '_STATE_S'] == 'SAFE'
            # flag &= ezca[(sysname + '-' + optic).upper() + '_COMMISH_STATUS'] == 1
            # flag &= ezca[(sysname + '-' + optic).upper() + '_COMMISH_MESSAGE'] != ''
        else:
            flag = True
        # flag &= ezca['FEC-' + str(fec) + '_SDF_DIFF_CNT'] == 0
        # flag &= int(ezca['FEC-' + str(fec) + '_STATE_WORD']) & 1024 == 0

        node = getmodel2hostname(ifo + model)
        print(node,flag,ifo,model)
        return flag

        #if ret:
        #    ezca[(sysname + '-' + optic).upper() + '_INSTALLED_DATE'] = datetime.now().strftime("%Y-%m-%d")
    #return 0


def buildoptic(model_in, make=False, makeinstall=False, restart=False):
    '''
    String :: model name
    Bool   :: make flg (default = Falase)
    Bool   :: make install flg (default = Falase)
    Bool   :: restart flg (default = Falase)
    -> Int :: exit code
    '''

    models = [(name, dcuid) for (name, dcuid) in FEMODELS if model_in[2:] in name]

    for (model, fec) in models:
        '''
        Condition:
            Guardian state == SAFE
            --Commish status == OFF--
            Commish message == NULL
            SDF diff == 0
            CFC bit == OFF
        '''

        if model_in[2:5] == 'vis':
            optic_tp = model_in[len('k1vis'):].lower()
            if optic_tp[-1:] == 't' or optic_tp[-1:] == 'p':
                optic = optic_tp[:-1]
            else:
                optic = optic_tp

            flag = ezca['GRD-VIS_' + optic.upper() + '_STATE_S'] == 'SAFE'
            # flag &= ezca[(sysname + '-' + optic).upper() + '_COMMISH_STATUS'] == 1
            # flag &= ezca[(sysname + '-' + optic).upper() + '_COMMISH_MESSAGE'] != ''
        else:
            flag = True
        # flag &= ezca['FEC-' + str(fec) + '_SDF_DIFF_CNT'] == 0
        # flag &= int(ezca['FEC-' + str(fec) + '_STATE_WORD']) & 1024 == 0

        fast_restart = ezca['FEC-' + str(fec) + '_SDF_DIFF_CNT'] == 0  # fast_restart !SDF:True, SDF: False

        node = getmodel2hostname(ifo + model)
        ret = False
        if flag == True:
            ret = buildmodel(ifo + model, node, make, makeinstall, restart, fast_restart)

        #if ret:
        #    ezca[(sysname + '-' + optic).upper() + '_INSTALLED_DATE'] = datetime.now().strftime("%Y-%m-%d")
    #return 0

def buildmodel(model, node, make=False, makeinstall=False, restart=False, fast_restart=False):
    '''
    String :: model name
    String :: node name
    Bool   :: make flg (default = Falase)
    Bool   :: make install flg (default = Falase)
    Bool   :: restart flg (default = Falase)
    -> Int :: exit code
    '''

    ssh=["ssh", "-oStrictHostKeyChecking=no", node]
    if make == True:
        cmd = 'cd /opt/rtcds/kamioka/k1/rtbuild/current/'
        cmd += ' && make ' + model
    if makeinstall == True:
        cmd = 'cd /opt/rtcds/kamioka/k1/rtbuild/current/'
        cmd += ' && make install-' + model
    if restart == True:
        if fast_restart == True:
            cmd = 'cd /opt/rtcds/userapps/release/cds/common/scripts'
            if 'iop' in model:
                cmd += ' && ./restart_iop_model.sh ' + model
            else:
                cmd += ' && ./restart_slave_model.sh ' + model
        else:
            cmd = 'cd /opt/rtcds/kamioka/k1/rtbuild/current/'
            cmd += ' && start' + model

    with open(LogFileDirectory+model+'.log','w') as output_file:

        print(ssh + [cmd])
        ret=subprocess.run(ssh + [cmd],
                            stdout = output_file,
                            text=True,
        #                       # stderr = subprocess.DEVNULL,
        )
        return ret.returncode == 0
#        return True
    return False



def buildresultcheck(model, make=False, makeinstall=False, restart=False):
    result = False
    with open(LogFileDirectory+model+'.log','r') as input_file:
        for line in input_file:
            if make and 'Successfully compiled' in line:
                result = True
            if makeinstall and 'Successfully compiled' in line:
                result = True
            if restart:
                result = True
    return result

if __name__ == "__main__":

#    buildoptic(hoge,'vis',True,False,False)
    print(buildresultcheck('k1vistmsy',False,True,False))
