#!/usr/bin/env python3
#! coding:utf-8

import buildlib
import threading
import time # sleep
import sys

ifo = 'k1'

build_target = []
build_status = [False] * len(build_target)

node_list = buildlib.getHostnameList()
node_value = [False] * len(node_list)

node_status = dict(zip(node_list,node_value))

sysname = 'vis'

thread_count = 0
thread_max = 5
thread_max_restart = 1

def thread_buildoptic(model, node, make=False, makeinstall=False, restart=False):
    comment = node + ' : ' 
    if make:
        comment = comment + 'make ' + model 
    if makeinstall:
        comment = comment + 'make install-' + model
    if restart:
        comment = comment + 'start' + model

    print('[start] '+comment)
    global thread_count

    buildlib.buildoptic(model, make, makeinstall, restart)
#    time.sleep(10.0)
    node_status[node] = False
    print('[end] '+comment)
    thread_count = thread_count - 1

    if buildlib.buildresultcheck(model,make,makeinstall,restart) == False:
        print('Error: '+model)
        sys.exit(1)

def buildfileParsor(buildlist):

    for index,line in enumerate(buildlist):

        if line[0] == '#' or len(line[:-1]) == 0:
            # comment line
            cmd = 'comment'
        elif line.find(' ') > 0:
            cmd, param = line.split(' ')
        else:
            cmd = line

        make = False
        makeinstall = False
        restart = False
        if cmd == 'make':
            pos = param.find('install-')
            if pos == -1:
                make = True
                model = param[:-1]
                #print('make ', model)
            else:
                makeinstall = True
                model = param[len('install-'):-1]
                #print('make install-', model)
        elif cmd.find('start') == 0:
            restart = True
            model = cmd[len('start'):-1]
            #print('start', model)
        elif cmd == 'comment':
            continue
        else:
            print('Unknown command line [',index+1,']')
            continue
            
        build_target.append({'model':model, 'make':make, 'makeinstall':makeinstall, 'restart': restart})
    
def main():
    global thread_count, thread_max, build_target, thread_max_restart
    args = sys.argv

    if len(args) == 2:
        filename = args[1]
        f = open(filename, 'r')
        buildlist = f.readlines()
        f.close()
        #print(buildlist)
    else:
        print('autobuild buildlist.txt')
        sys.exit(1)

    buildfileParsor(buildlist)

#    print(build_target)
    
    build_status = [False] * len(build_target)

    status = True
    build_step = 0 # 0:Check 1: make, 2: make install, 3: restart.
    while status == True:

        for index, build in enumerate(build_target):
            #print(index, optic)


            if build_step == 0:
                model = build['model']
                if buildlib.isbuildcheck(model) == False:
                    print('Please Check Suspension status. [ ' + build['model'] + ' ]')
                    sys.exit(1)

            else:
            #print(index, build)
                if ((build_step != 3) and (thread_count < thread_max)) or (thread_count < thread_max_restart):
                    #models = buildlib.getmodels(sysname,optic)
                    #for (model, fec) in models:
                    model = build['model']
                    make = build['make']
                    makeinstall = build['makeinstall']
                    restart = build['restart']

                    call_flg = False
                    if build_step == 1 and make == True:
                        call_flg = True

                    if build_step == 2 and makeinstall == True:
                        call_flg = True

                    if build_step == 3 and restart == True:
                        call_flg = True

                    if call_flg:
                    #  node = buildlib.getmodel2hostname(ifo + model)
                        node = buildlib.getmodel2hostname(model)
                        #print(node_status[node] )
                        if node_status[node] == False and build_status[index] == False:
                            node_status[node] = True
                            build_status[index] = True
                            thread = threading.Thread(target=thread_buildoptic, args=(model, node, make, makeinstall, restart) )
                            thread.start()
                            thread_count = thread_count + 1
                else:
                    pass

        if build_step == 0:
            status = True
        else:
            status = False
            for value in build_status:
                if value == False:
                    status = True

        if status == True and thread_count == 0 and build_step < 3:
            build_step = build_step + 1

        time.sleep(1.0)


if __name__ == '__main__':

    main()
    
