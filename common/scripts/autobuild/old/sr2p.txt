# ETMX
# make k1visetmxt
# make k1visetmxp
# make install-k1visetmxt
# make install-k1visetmxp
# startk1visetmxt
# startk1visetmxp

# ETMY
# make k1visetmyt
# make k1visetmyp
# make install-k1visetmyt
# make install-k1visetmyp
# startk1visetmyt
# startk1visetmyp

# ITMX
# make k1visitmxt
# make k1visitmxp
# make install-k1visitmxt
# make install-k1visitmxp
# startk1visitmxt
# startk1visitmxp

# ITMY
# make k1visitmyt
# make k1visitmyp
# make install-k1visitmyt
# make install-k1visitmyp
# startk1visitmyt
# startk1visitmyp

# BS
# make k1visbst
# make k1visbsp
# make install-k1visbst
# make install-k1visbsp
# startk1visbst
# startk1visbsp

# SRM
# make k1vissrmt
# make k1vissrmp
# make install-k1vissrmt
# make install-k1vissrmp
# startk1vissrmt
# startk1vissrmp

# SR2
# make k1vissr2t
make k1vissr2p
# make install-k1vissr2t
make install-k1vissr2p
# startk1vissr2t
startk1vissr2p

# SR3
# make k1vispr3t
# make k1vispr3p
# make install-k1vispr3t
# make install-k1vispr3p
# startk1vissr3t
# startk1vissr3p

# PRM
# make k1visprmt
# make k1visprmp
# make install-k1visprmt
# make install-k1visprmp
# startk1visprmt
# startk1visprmp

# PR2
# make k1vispr2t
# make k1vispr2p
# make install-k1vispr2t
# make install-k1vispr2p
# startk1vispr2t
# startk1vispr2p

# PR3
# make k1vispr3t
# make k1vispr3p
# make install-k1vispr3t
# make install-k1vispr3p
# startk1vispr3t
# startk1vispr3p

# MCI
# make k1vismci
# make install-k1vismci
# startk1vismci

# MCE
# make k1vismce
# make install-k1vismce
# startk1vismce

# MCO
# make k1vismco
# make install-k1vismco
# startk1vismco

# IMMT1
# make k1visimmt1
# make install-k1visimmt1
# startk1visimmt1

# IMMT2
# make k1visimmt2
# make install-k1visimmt2
# startk1visimmt2

# OMMT1
# make k1visommt1
# make install-k1visommt1
# startk1visommt1

# OMMT2
# make k1visommt2
# make install-k1visommt2
# startk1visommt2
