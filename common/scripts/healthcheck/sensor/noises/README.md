

# Noises

## Files
TypeA_F3_GAS.txt
 * exported from /kagra/Dropbox/Measurements/VIS/LVDT/GAS_LVDTs/2021/0203/GAS_LVDTs_1121.xml
 * reference klog: xxxx
 * col1 : Sensor noise
 * col2 : ADC noise

TypeA_BF_GAS.txt
 * exported from /kagra/Dropbox/Measurements/VIS/LVDT/GAS_LVDTs/2021/0120/GAS_LVDTs_1626.xml
 * reference klog: 15803
 * col1 : Sensor noise
 * col2 : ADC noise
 * col3 : Free
