/* RAMP_BIAS.c Function: RAMP_P5.c
 * 
 * This function applies a smooth ramp for bias changes
 * It is a 5th order polynomial.
 *
 * Inputs: 
 *
 * (1) double desired_val: value we want to ramp to, or hold 
 * (2) double T_ramp:  ramp time in seconds
 * 	
 * Outputs: 
 *
 * (1) double output_val: the current output value
 * (2) double state: 0 = ramping, 1= holding
 *	   
 * Authors: BTL 
 * April 30 - May 2013
 * see T1300510 for a derivation of the ramp - BTL June 12, 2013
 */

#define MODEL_RATE FE_RATE

typedef enum {HOLDING, RAMPING} RampStates;


void RAMP_P5(double *argin, int nargin, double *argout, int nargout){	
	static int RampTimer = 0;  // How far along the ramp are we, in cycles
	static int TotalRampCycles = 0;  // Number of cycles in the ramp
	static RampStates CurrentState = HOLDING;
	static int FirstCycle = 1;  // 1 will reinitialize things
	static double PreviousInput; 
	static double PreviousOutput;
	static double FinalOutput; // end value for the ramp
	double ThisOutput;	
	double Tramp;  // ramptime (sec) read only on new ramp start;
	static double RpC[6];  // these are the polynomial Ramp Coefs.
	double Xdiff;  // Total change for the ramp
	double Vmax;   // max velocity, computed from dX and dT
	double tt;     // time from ramp start, but scaled as -T/2 -> T/2.

	// Start by reading the inputs, we only read the ramp time on input changes.
	double ThisInput = argin[0];

	// on the first cycle, set the output = the input, and end
	if(FirstCycle == 1){
		ThisOutput     = ThisInput;
		FinalOutput    = ThisInput;
		// PreviousInput  = ThisInput;
		// PreviousOutput = ThisOutput;
		FirstCycle     = 0;
		CurrentState   = HOLDING;
	} else {
		if(ThisInput != PreviousInput){
			// start a new ramp
			Tramp     = (double) argin[1];
			if (Tramp < 0)   {Tramp = 0.0;}
			if (Tramp > 100) {Tramp = 100.0;}
			RampTimer = 0;
			TotalRampCycles = (int) (MODEL_RATE * Tramp);
			FinalOutput   = ThisInput;
			PreviousInput = ThisInput;
			CurrentState  = RAMPING;
			Xdiff = (double) FinalOutput - PreviousOutput;
			Vmax = (1.875) * Xdiff/Tramp;
			// RC are the Ramp Coefficients
			RpC[0] = PreviousOutput + (0.5 * Xdiff);	
			RpC[1] = Vmax;
			RpC[2] = 0.0;
			RpC[3] = (-2.6666666667/(Tramp* Tramp)) * Vmax;
			RpC[4] = 0.0;
			RpC[5] = (3.20/(Tramp*Tramp*Tramp*Tramp)) * Vmax; 
		}
		switch(CurrentState){
		case RAMPING: 
			RampTimer++;
			// make this back into a time which goes from -T/2 to +T/2;
			tt = (double) 2*RampTimer - TotalRampCycles;
			tt = (0.5 * tt)/(1.0 * MODEL_RATE);  // cast to double before the divide
			// RC[5]*tt^5 + RC[4]*tt^4 + ... RC[0] 
			ThisOutput = ((((RpC[5]*tt + RpC[4])*tt + RpC[3])*tt + RpC[2])*tt + RpC[1])*tt + RpC[0];
			if(RampTimer >= TotalRampCycles){
				ThisOutput   = FinalOutput;
				CurrentState = HOLDING;
			} else {
				CurrentState = RAMPING;
			}
		break;
		case HOLDING:
			ThisOutput = FinalOutput;
		break;
		}
	}
	// setup for the next cycle;
	PreviousInput  = ThisInput;
	PreviousOutput = ThisOutput;

	// Set the outputs: Ramp value and current state
	argout[0] = ThisOutput;
	// Output the int value of the current state
	argout[1] = CurrentState; 

	// Testing ouputs
	// argout[2] = RpC[0];
	// argout[3] = RpC[1];
	// argout[4] = RpC[2];
	// argout[5] = RpC[3];
	// argout[6] = RpC[4];
	// argout[7] = RpC[5];
	// argout[8] = tt;   
}
