import guardian
from guardian import GuardState,GuardStateDecorator
import sdflib,cdslib

__,OPTIC = SYSTEM.split('_')
#optic = OPTIC.lower()

state = 'INIT'
request = 'SAFE'
nominal = 'ALIGNED'

# --------------------------------------------------------------------
def visfilt(stage,func,dofs,filts,gain):
    '''
    '''
    ramp = []
    for dof in dofs:
        filtname = 'VIS-{0}_{1}_{2}_{3}'.format(OPTIC,stage,func,dof)
        filt = ezca.get_LIGOFilter(filtname)
        if gain==0:
            filt.ramp_gain(gain,trampdict[stage],False)
            if not filt.is_gain_ramping():
                filt.turn_off(*filts)
        elif gain==-1 or gain +1: # Fix me. remove +1
            filt.turn_on(*filts)
            filt.ramp_gain(gain,trampdict[stage],False)                        
        else:
            raise ValueError('!')

        ramp += [not filt.is_gain_ramping()]
    return ramp

def is_isolated():
    # should read RMS value.
    l = abs(ezca['VIS-{0}_IP_IDAMP_L_INMON'.format(OPTIC)]) < 5
    t = abs(ezca['VIS-{0}_IP_IDAMP_T_INMON'.format(OPTIC)]) < 5
    y = abs(ezca['VIS-{0}_IP_IDAMP_Y_INMON'.format(OPTIC)]) < 5
    #return l and t and y
    notify('[FIXME] Please define the requirement of the Isolated state')
    return True 


def is_damped():
    # # should read RMS value.
    # l = abs(ezca['VIS-{0}_IM_DAMP_L_INMON'.format(OPTIC)]) < 5
    # t = abs(ezca['VIS-{0}_IM_DAMP_T_INMON'.format(OPTIC)]) < 5
    # v = abs(ezca['VIS-{0}_IM_DAMP_V_INMON'.format(OPTIC)]) < 5
    # r = abs(ezca['VIS-{0}_IM_DAMP_R_INMON'.format(OPTIC)]) < 5
    # p = abs(ezca['VIS-{0}_IM_DAMP_P_INMON'.format(OPTIC)]) < 5
    # y = abs(ezca['VIS-{0}_IM_DAMP_Y_INMON'.format(OPTIC)]) < 5    
    # return l and t and v and r and p and y
    return True


def sus_type_is():
    if OPTIC in ['ETMX','ETMY','ITMX','ITMY']:
        return 'Type-A'
    elif OPTIC in ['BS','SRM','SR2','SR3']:
        return 'Type-B'
    elif OPTIC in ['PRM','PR2','PR3']:
        return 'Type-Bp'
    elif OPTIC in ['MCI','MCO','MCE','IMMT1','IMMT2']:
        return 'Type-Ci'
    elif OPTIC in ['OSTM','OMMT1','OMMT2']:
        return 'Type-Co'
    else:    
        return False

def has_tower():
    if OPTIC in ['ETMX','ETMY','ITMX','ITMY',
                 'BS','SRM','SR2','SR3',
                 'PRM','PR2','PR3']:
        return True
    else:
        return False
    

def large_rms():
    return ezca['VIS-{0}_TM_WDMON_CURRENTTRIG'.format(OPTIC)]==1

def tripped():
    wd = ezca['VIS-{0}_TM_WDMON_STATE'.format(OPTIC)]==2
    dk = ezca['VIS-{0}_PAY_DACKILL_STATE'.format(OPTIC)]==0
    if has_tower():
        dk = dk or ezca['VIS-{0}_TWR_DACKILL_STATE'.format(OPTIC)]==0
        
    return wd or dk

class WDcheck(GuardStateDecorator):
    def pre_exec(self):
        if tripped():
            return 'TRIPPED'

class IsolatedCheck(GuardStateDecorator):
    def pre_exec(self):
        if not is_isolated():
            return 'ISOLATING'

class DampedCheck(GuardStateDecorator):
    def pre_exec(self):
        if not is_damped():
            return 'DAMPING'
        
        
class SAFE(GuardState):
    request = True
    index = 3

    def main(self):
        pass
    
    def run(self):
        return True

class TRIPPED(GuardState):
    request = False
    index = 2

    def main(self):
#        ezca['VIS-{0}_MASTERSWITCH'.format(OPTIC)] = 0
#        ezca['GRD-VIS_IMMT1_REQUEST'] = 'SAFE'
        pass

    def run(self):
#        if large_rms():
#            notify('Please reduce the RMS.')
#        else:
#            notify('Please release the WD and DK')
#        return not tripped()
        return True

class INIT(GuardState):
    request = True

    def main(self):
        pass

    def run(self):
        return True

class ALIGNING(GuardState):
    request = False
    index = 998
    
    @WDcheck
    def main(self):
        pass

    def run(self):
        for dof in ['P','Y']:
            filtname = 'VIS-{0}_TM_OPTICALIGN_{1}'.format(OPTIC,dof)
            filt = ezca.get_LIGOFilter(filtname)
            filt.turn_on('OFFSET')            
#            if dof=='P':
#                filt.ramp_gain(-1,0.5,False)
#            else:
#                filt.ramp_gain(1,0.5,False)
        # SDF
        fec = cdslib.ezca_get_dcuid('K1VIS'+OPTIC)
        sdflib.restore(fec,'aligned')

        return True

class DISABLE_ALIGN(GuardState):
    request = False
    index = 999

    @WDcheck
    def main(self):
        pass

    def run(self):
        for dof in ['P','Y']:
            filtname = 'VIS-{0}_TM_OPTICALIGN_{1}'.format(OPTIC,dof)
            filt = ezca.get_LIGOFilter(filtname)
            filt.turn_off('OFFSET')
#            filt.ramp_gain(0,0.5,False)
        return True

class ALIGNED(GuardState):
    request = True
    index = 1000

    @WDcheck
    def main(self):
        pass

    @WDcheck
    #@OLcheck
    def run(self):
        return True

class MISALIGNING(GuardState):
    request = False
    index = 898

    @WDcheck
    #@OLcheck    
    def main(self):
        pass

    @WDcheck
    #@OLcheck
    def run(self):
        # SDF
        fec = cdslib.ezca_get_dcuid('K1VIS'+OPTIC)
        sdflib.restore(fec,'misaligned')         
        return True

class DISABLE_MISALIGN(GuardState):
    request = False
    index = 899

    @WDcheck
    #@OLcheck    
    def main(self):
        pass

    @WDcheck
    #@OLcheck
    def run(self):
        return True

class MISALIGNED(GuardState):
    request = True
    index = 900

    @WDcheck
    #@OLcheck    
    def main(self):
        pass

    @WDcheck
    #@OLcheck
    def run(self):
        return True

class DAMPING(GuardState):
    request = False
    index = 498
    
    @WDcheck
    #@OLcheck    
    def main(self):
        self.counter = 0                        
        ezca['VIS-{0}_PAY_MASTERSWITCH'.format(OPTIC)] = 1        

    @WDcheck
    #@OLcheck
    def run(self):
        # 0: ENGAGE_GAS_DAMP
        if self.counter==0:
            if sus_type_is() in ['Type-A','Type-B','Type-Bp']:
                visfilt('F1','DAMP',['GAS'],['FM4','INPUT'],-1)
                visfilt('F2','DAMP',['GAS'],['FM4','INPUT'],-1)
                visfilt('F3','DAMP',['GAS'],['FM4','INPUT'],-1)
                visfilt('BF','DAMP',['GAS'],['FM4','INPUT'],-1)
            elif sus_type_is() in ['Type-Ci','Type-Co']:
                notify('{0} does not have GAS. Skip.'.format(OPTIC))
            else:
                raise ValueError('hoge!')
            self.counter += 1
        # 1: ENGAGE_BF_DAMP
        if self.counter==1:            
            if sus_type_is() in ['Type-A','Type-B','Type-Bp']:
                visfilt('BF','DAMP',['L','T','Y','R','P','V'],['FM4','INPUT'],-1)
            elif sus_type_is() in ['Type-Ci','Type-Co']:
                notify('{0} does not have BF stage. Skip.'.format(OPTIC))                
            else:
                raise ValueError('hoge!')
            self.counter += 1
        # 2: ENGAGE_MN_DAMP
        if self.counter==2:
            pass
            self.counter += 1
        # 3: ENGAGE_IM_DAMP
        if self.counter==3:
            if sus_type_is() in ['Type-Bp']:
                visfilt('IM','DAMP',['L','T','Y','R','P','V'],['FM4','INPUT'],-1)
            elif sus_type_is() in ['Type-Ci','Type-Co']:
                notify('{0} does not have IM stage. Skip.'.format(OPTIC))
            else:
                raise ValueError('!')
            self.counter += 1
        # 4: ENGAGE_TM_OLDAMP
        if self.counter==4:
            pass        
            if is_damped():
                return True   
        else:                        
            raise ValueError('!')
        

class DISABLE_DAMP(GuardState):
    request = False
    index = 499
    
    @WDcheck
    #@OLcheck    
    def main(self):
        self.counter = 0

    @WDcheck
    #@OLcheck
    def run(self):            
        #0: DISABLE_TM_OLDAMP
        if self.counter==0:
            self.counter += 1
        #1: DISABLE_IM_DAMP
        elif self.counter==1:                        
            if sus_type_is() in ['Type-Bp']:
                imramp = visfilt('IM','DAMP',['L','T','Y','R','P','V'],['FM4','INPUT'],0)
                if all(imramp):
                    self.counter += 1
            else:
                raise ValueError('hoge!')
        #2: DISABLE_MN_DAMP
        if self.counter==2:
            self.counter += 1
        #3: DISABLE_BF_DAMP
        if self.counter==3:
            r0 = visfilt('F0','DAMP',['GAS'],['FM4','INPUT'],0)
            r1 = visfilt('F1','DAMP',['GAS'],['FM4','INPUT'],0)
            r2 = visfilt('F2','DAMP',['GAS'],['FM4','INPUT'],0)
            r3 = visfilt('F3','DAMP',['GAS'],['FM4','INPUT'],0)
            r4 = visfilt('BF','DAMP',['GAS'],['FM4','INPUT'],0)
            if all(r0) and all(r1) and all(r2) and all(r3) and all(r4):
                self.counter += 1
        #4: DISABLE_GAS_DAMP
        if self.counter==4:
            self.counter += 1
        #5: Close Payload Masterswitch
        elif self.counter==5:
            ezca['VIS-{0}_PAY_MASTERSWITCH'.format(OPTIC)] = 0        
            return True

class DAMPED(GuardState):
    request = True
    index = 500

    @WDcheck
    #@OLcheck    
    def main(self):
        pass

    @WDcheck
    #@OLcheck    
    def run(self):
        return True

# ------------------------------------------------------------------------------

trampdict = {'IP':30,
             'F0':10,
             'F1':10,
             'F2':10,
             'F3':10,
             'SF':10,
             'BF':10,
             'IM':5}
    
class ISOLATING(GuardState):
    request = False
    index = 98

    @WDcheck    
    def main(self):
        '''
        Open only the tower masterswitch to engage IP and BF controls
        '''
        self.counter = 0                
        ezca['VIS-{0}_TWR_MASTERSWITCH'.format(OPTIC)] = 1        
        pass
    
    @WDcheck
    def run(self):
        '''
        a
        '''
        # 0: IP_ISOLATED        
        if self.counter==0:
            if sus_type_is() in ['Type-A','Type-B','Type-Bp']:
                visfilt('IP','IDAMP',['L','T','Y'],['FM4','INPUT'],+1)
                visfilt('IP','IDAMP',['L','T','Y'],['FM3','INPUT'],+1)                
            elif sus_type_is() in ['Type-Ci','Type-Co']:
                notify('{0} does not have IP stage. Skip.'.format(OPTIC))
            else:
                raise ValueError('huge!')
            self.counter += 1
        # 1: TF_ISOLATED(SC)
        if self.counter==1:
            if sus_type_is() in ['Type-A','Type-B','Type-Bp']:
                visfilt('F0','DAMP',['GAS'],['FM4','INPUT'],-1)
                visfilt('SF','DAMP',['GAS'],['FM4','INPUT'],-1)                
            elif sus_type_is() in ['Type-Ci','Type-Co']:
                notify('{0} does not have Top Filter. Skip.'.format(OPTIC))
            else:
                raise ValueError('hoge!')
            if is_isolated():
                return True            
        else:
            raise ValueError('!')
        
        

class TO_SAFE(GuardState):
    request = False
    index = 99
    
    @WDcheck        
    def main(self):
        self.counter = 0
        self.ramp = []

    @WDcheck
    def run(self):
        if self.counter == 0:
            # SDF
            for PART in ['T','P']:
                fec = cdslib.ezca_get_dcuid('K1VIS'+OPTIC+PART)
                sdflib.restore(fec,'safe')
            self.counter += 1
            
        elif self.counter==1:
            is_ramped_count = 0
            if sus_type_is() in ['Type-B','Type-A','Type-Bp']:
                bframp = visfilt('BF','DAMP',['L','T','Y','R','P','V'],['FM4','INPUT'],0)
                ipramp = visfilt('IP','IDAMP',['L','T','Y'],['FM3','FM4','INPUT'],0)
                tfgasramp = visfilt('SF','DAMP',['GAS'],['FM4','INPUT'],0)
                if all(bframp) and all(ipramp) and all(tfgasramp):
                    self.counter +=1
            elif sus_type_is()=='Type-Ci':
                # Fix me..                
                pass
                # for dof in ['P','Y']:
                #     filtname = 'VIS-{0}_TM_OPTICALIGN_{1}'.format(OPTIC,dof)
                #     filt = ezca.get_LIGOFilter(filtname)
                #     if filt.is_offset_ramping() == False:
                #         is_ramped_count += 1
                # if is_ramped_count == 2:
                #     ezca['VIS-{0}_MASTERSWITCH'.format(OPTIC)] = 0
                #     return True                                    
            else:
                raise ValueError('!!!!')            
        elif self.counter==2:
            ezca['VIS-{0}_TWR_MASTERSWITCH'.format(OPTIC)] = 0
            return True
        else:
            raise ValueError('hoge!')                                        


class ISOLATED(GuardState):
    request = True
    index = 100

    @WDcheck        
    def main(self):
        pass  

    @WDcheck
    @IsolatedCheck
    def run(self):
        return True    
    
edges = [('INIT','SAFE'),
         ('DAMPED','ALIGNING'),
         ('ALIGNING','ALIGNED'),
         ('ALIGNED','DISABLE_ALIGN'),
         ('DISABLE_ALIGN','DAMPED'),
         ('DAMPED','MISALIGNING'),
         ('MISALIGNING','MISALIGNED'),
         ('MISALIGNED','DISABLE_MISALIGN'),
         ('DISABLE_MISALIGN','DAMPED'),         
         ('ISOLATED','DAMPING'),
         ('DAMPING','DAMPED'),
         ('DAMPED','DISABLE_DAMP'),
         ('DISABLE_DAMP','ISOLATED'),
         ('SAFE','ISOLATING'),
         ('ISOLATING','ISOLATED'),
         ('ISOLATED','TO_SAFE'),
         ('TO_SAFE','SAFE'),
         ('TRIPPED','TO_SAFE'),         
]
