# VIS Userapps
## Directory Structure
./common : common files in VIS, which is used from ./k1.
 - guardian
 - medm
 - models
 - scripts
 - src

./k1 : files running in KAGRA system.
 - burtfiles
 - filterfiles
 - guardian
 - medm
 - models
 - scripts

./miyoconda37.yml: conda environment file for userapps
